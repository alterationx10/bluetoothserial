package com.markslaboratory.bluetoothserial;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends Activity {

    // The buttons from our layout
    Button btnConnect;
    Button btnDisconnect;
    Button btnRead;
    Button btnWrite;

    // Our bluetooth service
    BluetoothService myService;

    // PUT THE MAC ADDRESS OF YOUR DEVICE HERE!
    // FORMAT MATTER!
    String MAC = "B8:FF:FE:B9:D2:89";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if(!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            genericDialog("Bluetooth isn't enabled... \n You might want to turn it on...");
        }

        // We need to bind to our service, so lets go ahead and do that
        ServiceConnection mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                genericDialog("Service connected! The app should work now :-)");
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                genericDialog("Service disconnected!");
            }
        };

        myService = new BluetoothService();
        // Don't forget we need to decalre the Service (and Bluetooth permission) in the manifest file.
        Intent myIntent = new Intent(getApplicationContext(), BluetoothService.class);
        bindService(myIntent, mConnection, Context.BIND_AUTO_CREATE);
        // Our service should bind now; We will see the dialogs above whe it's all ready;


        // Hook up all of our buttons
        btnConnect = (Button)findViewById(R.id.btnConnect);
        btnDisconnect = (Button)findViewById(R.id.btnDisconnect);

        btnRead = (Button)findViewById(R.id.btnRead);
        btnWrite = (Button)findViewById(R.id.btnWrite);


        // Do things when the buttons are clicked
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myService.btConnect(MAC, new BluetoothService.BluetoothConnectCallback() {
                    @Override
                    public void doOnConnect() {
                        genericDialog("Successfully connected!");
                    }

                    @Override
                    public void doOnCOnnectionFailed() {
                        genericDialog("Failed to connect!");
                    }

                    @Override
                    public void doOnDisconnect() {
                        // Nothing to do here
                    }
                });
            }
        });

        btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myService.btDisconnect(new BluetoothService.BluetoothConnectCallback() {
                    @Override
                    public void doOnConnect() {
                        // Nothing to do here
                    }

                    @Override
                    public void doOnCOnnectionFailed() {
                        // Nothing to do here
                    }

                    @Override
                    public void doOnDisconnect() {
                        genericDialog("Successfully disconnected!");
                    }
                });
            }
        });

        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myService.readData(new BluetoothService.DataReadCallback() {
                    @Override
                    public void doOnDataRead(byte[] theData) {
                        if (theData.length == 0) {
                            genericDialog("There was no data to read.");
                            return;
                        }
                        // Make a hex string
                        String msg = "I read the following hex string:\n";
                        for (int i=0; i < theData.length; i++) {
                            msg += Integer.toHexString(theData[i] & 0xff) + " ";
                        }
                        genericDialog(msg);
                    }

                    @Override
                    public void doOnReadFail() {
                        genericDialog("I failed to read any data!");
                    }
                });
            }
        });

        btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] data = customMethodForTesting();
                boolean result = myService.writeData(data);
                if (result) {
                    Toast.makeText(MainActivity.this,"Data was written to device",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this,"Error writing to device!",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private byte[] customMethodForTesting() {
        //This is a test method for me, with custom hardware...
        int red = new Random().nextInt(256);
        int green = new Random().nextInt(256);
        int blue = new Random().nextInt(256);

        byte[] rgbValues = {0x50, 0x08, 0x15,
                (byte) red, (byte) green, (byte) blue,
                (byte) red, (byte) green, (byte) blue,
                0x00};
        return rgbValues;
    }

    public void genericDialog(String msg) {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("A Wild Message Appears!");
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}
